const defaultTheme = require('tailwindcss/defaultTheme')
const formsPlugin = require('@tailwindcss/forms')
const aspectRatioPlugin = require('@tailwindcss/aspect-ratio')
const typographyPlugin = require('@tailwindcss/typography')

module.exports = {
  purge: ['src/**/*.js'],
  darkMode: false,
  theme: {
    extend: {
      fontFamily: {
        sans: ['CircularStd-medium', ...defaultTheme.fontFamily.sans],
        sans2: ['DM Sans', ...defaultTheme.fontFamily.sans],
        sans3: ['Inter', ...defaultTheme.fontFamily.sans],
        serif: ['Roboto', ...defaultTheme.fontFamily.serif]
      },
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      white: '#FFFFFF',
      indigo: {
        alpha1: "#9a2af257",
        dark: '#3E2F99',
        DEFAULT: '#5341C5',
        lighter: '#B7AFED',
        lightest: '#F0EDFF',
      },
      black: {
        DEFAULT: '#000000',
        light: '#1818196B'
      },
      white: {
        DEFAULT: '#FFFFFF',
        dark: '#FAFBFF'
      },
      gray: {
        darker: '#333333',
        dark: '#6A686F',
        DEFAULT: '#505963',
        light: '#C4C4C4',
        lightest: '#E6E7E9'
      },
      purple: {
        light: '#A3AED0',
      },
      orange: {
        light: '#FFE2E1'
      },
      yellow: {
        DEFAULT: '#FCDF81'
      },
      blue: {
        light: '#B6D9F0'
      },
      green: {
        DEFAULT: '#09873C'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [formsPlugin, aspectRatioPlugin, typographyPlugin],
}
