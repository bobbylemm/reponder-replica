export default {
    jwt: {
        AUTH_TOKEN_NAME: 'authToken'
    },
    services: {
        AUTH_SERVICE_V1: 'http://18.132.14.226:30001/api/v1/',
        DANGER_SERVICE_V1: 'http://18.132.14.226:30001/api/v1/' 
    }
}