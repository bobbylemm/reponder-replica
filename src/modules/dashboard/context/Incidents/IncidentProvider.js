import React, { useReducer, createContext } from "react";
import PropTypes from "prop-types";
import { find, propEq, isEmpty, assoc, map } from "ramda";

import { incidents, agents } from "../../utils/testData";

const defaultState = {
  data: {
    currentlyViewedIncident: null, // what single incident has been navigated to
    viewIncident: false, // view single incident page
    viewAgent: false, // view single agent page
    currentlyViewedAgent: null,
    agents,
    incidents: {
      pending: [],
      started: [],
      currentlyViewedTab: 'pending' // what tab on the incidents page is being viewed
    },
  },
  dispatch: null,
};

const ProjectDetailsContext = createContext(defaultState);

const getIncident = (id, type) => {
  return ;
};

const incidentsReducer = (state, action) => {
  switch (action.type) {
    case "setIncidents": {
      const data = action?.payload?.data;
      const type = action?.payload?.type

      return {
        ...state,
        data: {
          ...state.data,
          incidents: {
            ...state.data.incidents,
            [type]: data,
            currentlyViewedTab: type
          }
        },
      };
    }
    case "viewIncident": {
      const incidentType = action?.payload?.type
      const incidentId = action?.payload?.id
      const incident = find(propEq("_id", incidentId), state.data.incidents[incidentType]);
      return {
        ...state,
        data: {
          ...state.data,
          currentlyViewedIncident: incident,
          viewIncident: !isEmpty(incident),
        },
      };
    }
    case "viewIncidents": {
      return {
        ...state,
        data: {
          ...state.data,
          currentlyViewedIncident: null,
          viewIncident: false,
        },
      };
    }
    case "processIncident": {
      const incidents = map((incident) => {
        if (incident?.id === action?.payload?.id) {
          incident = assoc("status", "active", incident);
        }
        return incident;
      }, state.data.incidents);

      return {
        ...state,
        data: {
          ...state.data,
          currentlyViewedIncident: assoc(
            "status",
            "active",
            state.data.currentlyViewedIncident
          ),
          incidents,
        },
      };
    }
    case "viewAgent": {
      const agent = find(propEq("id", action?.payload?.id), agents);
      return {
        ...state,
        data: {
          ...state.data,
          currentlyViewedAgent: agent,
          viewAgent: !isEmpty(agent),
        },
      };
    }
    case "viewAgents": {
      return {
        ...state,
        data: { ...state.data, currentlyViewedAgent: null, viewAgent: false },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

const IncidentsProvider = ({ children }) => {
  const [data, dispatch] = useReducer(incidentsReducer, defaultState);

  return (
    <ProjectDetailsContext.Provider value={{ data, dispatch }}>
      {children}
    </ProjectDetailsContext.Provider>
  );
};

const useIncidentsContext = () => {
  const context = React.useContext(ProjectDetailsContext);
  if (context === undefined) {
    throw new Error("useCount must be used within a CountProvider");
  }
  return context;
};

IncidentsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export { IncidentsProvider, useIncidentsContext };
