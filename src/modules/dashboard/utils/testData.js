export const incidents = [
  {
    id: 1,
    incidentType: "robbery",
    victim: "Aleena",
    time: "5 minutes",
    status: "pending",
    location: "Cluster C, 1004, Victoria Island, Lagos state",
  },
  {
    id: 2,
    incidentType: "robbery",
    victim: "Miguel",
    time: "7 minutes",
    status: "active",
    location: "Phoenix way, 1004, Victoria Island, Lagos state",
  },
  {
    id: 3,
    incidentType: "robbery",
    victim: "Sam",
    time: "10 minutes",
    status: "pending",
    location: "Sheringtin road, 1004, Victoria Island, Lagos state ",
  },
];

export const agents = [
  {
    id: 1,
    agent: "Israel madagascar",
    email: "israel@email.com",
    time: "21 mins away",
    stat: { completedTasks: 5, completionRate: "95" },
  },
  {
    id: 2,
    agent: "Tom miguel",
    email: "tom@email.com",
    time: "11 mins away",
    stat: { completedTasks: 15, completionRate: "85" },
  },
  {
    id: 3,
    agent: "Reece vies",
    email: "reece@email.com",
    time: "31 mins away",
    stat: { completedTasks: 2, completionRate: "90" },
  },
];

export const testMessageData = [
  {
    id: 1,
    text: "Hello there",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
  {
    id: 2,
    text: "How are you doing",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
  {
    id: 3,
    text: "Hello how can I help you",
    sentBy: "agent",
    userName: "Tomi",
    createdAt: "12/12/2021",
  },
  {
    id: 4,
    text: "Hello there",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
  {
    id: 5,
    text: "How are you doing",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
  {
    id: 6,
    text: "Hello there",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
  {
    id: 7,
    text: "How are you doing",
    sentBy: "user",
    userName: "Aleena",
    createdAt: "12/12/2021",
  },
];
