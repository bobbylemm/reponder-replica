import React from "react";
import pluck from "ramda/src/pluck";
import isEmpty from "ramda/src/isEmpty";
import toast from "react-hot-toast";
import { AnimatePresence } from "framer-motion";
import Map from "modules/common/components/Map";
import {
  IncidentsProvider,
  useIncidentsContext,
} from "../../context/Incidents/IncidentProvider";
import Incidents from "./Incidents";
import Incident from "./Incident";
import ChatCard from "modules/common/components/Chat";
import PageLeftInFade from "modules/common/components/Animations/PageLeftInFade";
import { testMessageData } from "../../utils/testData";

const Wrapper = () => {
  const {
    data: {
      data: { viewIncident, incidents, currentlyViewedIncident },
    },
    dispatch,
  } = useIncidentsContext();

  const notify = () =>
    toast.custom((t) => (
      <div
        className={`${
          t.visible ? "animate-enter" : "animate-leave"
        } max-w-md w-full bg-white shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5`}
      >
        <div className="flex-1 w-0 p-4">
          <div className="flex items-start">
            <div className="flex-shrink-0 pt-0.5">
              <img
                className="h-10 w-10 rounded-full"
                src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixqx=6GHAjsWpt9&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.2&w=160&h=160&q=80"
                alt=""
              />
            </div>
            <div className="ml-3 flex-1">
              <p className="text-sm font-medium text-gray-900">Emilia Gates</p>
              <p className="mt-1 text-sm text-gray-500">
                Sure! 8:30pm works great!
              </p>
            </div>
          </div>
        </div>
        <div className="flex border-l border-gray-200">
          <button
            onClick={() => toast.dismiss(t.id)}
            className="w-full border border-transparent rounded-none rounded-r-lg p-4 flex items-center justify-center text-sm font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none focus:ring-2 focus:ring-indigo-500"
          >
            Close
          </button>
        </div>
      </div>
    ));

  return (
    <>
      <div className="bg-white absolute left-0 top-0 bottom-0 w-2/6 z-10 py-6">
        <AnimatePresence exitBeforeEnter>
          {!viewIncident && (
            <PageLeftInFade key="child 1">
              <Incidents dispatch={dispatch} incidents={incidents} />
            </PageLeftInFade>
          )}
          {viewIncident && (
            <PageLeftInFade key="child 2">
              <Incident />
            </PageLeftInFade>
          )}
        </AnimatePresence>
      </div>
      <div className="absolute bottom-0 right-0 z-10 p-10 w-2/6">
        <ChatCard messages={testMessageData} />
      </div>
      <Map
        focusOnCoordinate={currentlyViewedIncident?.location}
        markersCoordinates={
          !isEmpty(incidents[incidents.currentlyViewedTab])
            ? pluck("location")(incidents[incidents.currentlyViewedTab])
            : []
        }
      />
    </>
  );
};

const IncidentsPage = () => {
  return (
    <div className="h-full w-full relative">
      <IncidentsProvider>
        <Wrapper />
      </IncidentsProvider>
    </div>
  );
};

export default IncidentsPage;
