import React, { useState } from "react";
import classNames from "classnames";
import path from "ramda/src/path";
import {
  ClockIcon,
  PencilIcon,
  PhoneIcon,
  MailIcon,
  PlusCircleIcon,
} from "@heroicons/react/solid";
import FirstAidIcon from "assets/icons/solid/first-aid.svg";
import MedalIcon from "assets/icons/solid/medal.svg";
import CarIcon from "assets/icons/solid/car.svg";
import PendingDoubleCheck from "assets/icons/outline/pending-double-check.svg";
import SuccessfulDoubleCheck from "assets/icons/outline/successful-double-check.svg";
import TransferIncident from "modules/common/components/Modals/TransferIncident";
import AssignIncident from "modules/common/components/Modals/AssignIncident";
import UpdateIncident from "modules/common/components/Modals/UpdateIncident";

const IncidentInfo = ({ incident, dispatch, agents }) => {
  const [showTransferIncident, setShowTransferIncident] = useState(false);
  const [showAssignIncident, setShowAssignIncident] = useState(false);
  const [showUpdateIncident, setShowUpdateIncident] = useState(false);
  const victim = `${incident?.user?.first_name || "N/A"} ${
    incident?.user?.last_name || "N/A"
  }`;
  const victimPhone = incident?.user?.phone_number || 'N/A'
  const victimEmail = incident?.user?.email || 'N/A'
  const incidentType = `${incident?.category?.name ?? "Unknown Incident"}`;
  const location =
    path(["street_address_types", 0, "formatted_address"], incident) ?? "N/A";
  const status = incident?.status ?? "n/a";

  return (
    <>
      <div className="p-6">
        <h2 className="text-2xl">{victim}</h2>
        <div className="flex items-center text-indigo">
          {status === "pending" ? (
            <span
              className="uppercase flex items-center mr-4 cursor-pointer"
              onClick={() => setShowAssignIncident(true)}
            >
              <PlusCircleIcon className="w-6 h-6 mr-1" /> Assign Incident
            </span>
          ) : (
            <span className="uppercase">{incidentType}</span>
          )}
          <span
            className={classNames(
              {
                "bg-orange-light": status === "pending",
                "bg-yellow": status === "started",
              },
              "inline-flex uppercase items-center px-2.5 py-1 border border-transparent text-sm font-medium rounded text-gray ml-4 my-3"
            )}
          >
            {status}
          </span>
        </div>

        <p className="break-words pr-8 text-gray">{location}</p>
        <ul className="list-none space-y-4 mt-4">
          <li>
            <button
              type="button"
              className="inline-flex items-center py-2 text-lg text-indigo leading-4 font-medium rounded-md bg-transparent"
            >
              <PencilIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
              Edit address
            </button>
          </li>
          <li className="flex">
            <ClockIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
            Today 4:15pm
          </li>
          <li className="flex">
            <FirstAidIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
            {victimPhone}
          </li>
        </ul>

        {status === "pending" ? (
          <ul className="list-none space-y-4 mt-6 pt-6 border-t border-b pb-6 border-gray-lightest text-gray space-y-6">
            <li className="flex">
              <PhoneIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
              {victimPhone}
            </li>
            <li className="flex">
              <MailIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
              {victimEmail}
            </li>
            <li className="flex">
              <MedalIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
              Free plan
            </li>
            <li className="flex">
              <CarIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
              Black Honda Civic - ABC 123XYZ
            </li>
          </ul>
        ) : null}
        {status === "started" ? (
          <div className="border-b pb-6 border-gray-lightest">
            <ul className="list-none space-y-4 mt-6 pt-6 border-t pb-6 border-gray-lightest text-gray space-y-6">
              <li className="flex items-center">
                <PendingDoubleCheck
                  className="-ml-0.5 mr-2 h-6 w-6"
                  aria-hidden="true"
                />
                <div className="flex flex-col">
                  <span>Incident reported</span>
                  <div className="flex">
                    <span>Aug 13 2020</span>
                    <span className="ml-2 text-gray-light">2:21 pm</span>
                  </div>
                </div>
              </li>
              <li className="flex items-center">
                <SuccessfulDoubleCheck
                  className="-ml-0.5 mr-2 h-6 w-6"
                  aria-hidden="true"
                />
                <div className="flex flex-col">
                  <span>Agent arrived at victim’s location</span>
                  <div className="flex">
                    <span>Aug 13 2020</span>
                    <span className="ml-2 text-gray-light">2:21 pm</span>
                  </div>
                </div>
              </li>
            </ul>
            <button className="text-indigo" onClick={() => null}>
              Show breakdowns
            </button>
          </div>
        ) : null}
        <div className="mt-6">
          <button
            onClick={() =>
              status === "pending"
                ? dispatch({
                    type: "processIncident",
                    payload: { id: incident?.id },
                  })
                : setShowTransferIncident(true)
            }
            type="button"
            className="flex w-full justify-center items-center py-3 border border-transparent text-lg font-medium rounded text-white bg-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            {status === "pending" ? "Start processing" : "Transfer Incident"}
          </button>
          <button
            onClick={() => setShowUpdateIncident(true)}
            type="button"
            className="flex w-full justify-center items-center py-3 mt-4 border border-transparent text-lg font-medium rounded bg-transparent text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Update
          </button>
        </div>
      </div>
      <TransferIncident
        setOpen={setShowTransferIncident}
        open={showTransferIncident}
        agents={agents}
      />
      <AssignIncident
        setOpen={setShowAssignIncident}
        open={showAssignIncident}
        incidentId={incident?._id}
      />
      <UpdateIncident
        setOpen={setShowUpdateIncident}
        open={showUpdateIncident}
        incidentId={incident?._id}
      />
    </>
  );
};

export default IncidentInfo;
