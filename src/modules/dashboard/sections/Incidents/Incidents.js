import React, {useState, useEffect} from "react";
import find from 'ramda/src/find'
import path from 'ramda/src/path'
import propEq from 'ramda/src/propEq'
import Tabs from "components/Tabs";
import PendingIncidents from "../PendingIncidents";
import ActiveIncidents from "../ActiveIncidents";
import useApiQuerySWR from "modules/common/hooks/useApiQuerySWR";
import configs from 'configs/apiConfigs'
import PeopleGroupSVG from "assets/icons/solid/people-group-rounded.svg";

const Incidents = ({dispatch, incidents}) => {
  const {data: statData, error: statError} = useApiQuerySWR(configs.services.DANGER_SERVICE_V1, 'dangers/count-status', {withAuthHeaders: true})
  const numberOfPendingDangers = find(propEq('status', 'pending'))(path(['data'], statData) || {})
  const numberOfStartedDangers = find(propEq('status', 'started'))(path(['data'], statData) || {})

  return (
    <>
      <div className="h-1/5 px-6">
        <h2 className="font-sans2 mb-4">Emergency Situations</h2>
        <div className="flex border border-gray-lightest py-6">
          <div className="flex border-r border-gray-lightest w-1/2 items-center px-4">
            <PeopleGroupSVG className="w-10 h-10" />
            <div className="flex flex-col ml-2">
              <span className="font-sans2">Pending</span>
              <span className="font-sans2">{path(['count'], numberOfPendingDangers) || 0}</span>
            </div>
          </div>
          <div className="flex w-1/2 items-center px-4">
            <PeopleGroupSVG className="w-10 h-10" />
            <div className="flex flex-col ml-2">
              <span className="font-sans2">Active</span>
              <span className="font-sans2">{path(['count'], numberOfStartedDangers) || 0}</span>
            </div>
          </div>
        </div>
      </div>
      <div className="h-4/5 flex flex-col">
        <Tabs
          items={[
            {
              title: "Pending",
              content: <PendingIncidents dispatch={dispatch} />,
            },
            { title: "Active", content: <ActiveIncidents dispatch={dispatch} /> },
            { title: "Abandoned", content: <div>Still in progress</div> },
          ]}
          defaultActiveIndex={0}
        />
      </div>
    </>
  );
};

export default Incidents;
