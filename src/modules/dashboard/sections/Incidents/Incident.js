import React from "react";
import { ArrowLeftIcon } from "@heroicons/react/solid";
import Tabs from "components/Tabs";
import IncidentInfo from "./IncidentInfo";
import AgentAssigned from "../Agent/AgentAssigned";
import { useIncidentsContext } from "../../context/Incidents/IncidentProvider";
import PageLeftInFade from "modules/common/components/Animations/PageLeftInFade";
import { agents } from "../../utils/testData";

const Incident = () => {
  const {
    data: {
      data: { viewAgent, currentlyViewedAgent, currentlyViewedIncident },
    },
    dispatch,
  } = useIncidentsContext();

  return (
    <div className="h-full flex flex-col">
      <div className="mb-6">
        <button
          onClick={() =>
            dispatch({ type: viewAgent ? "viewAgents" : "viewIncidents" })
          }
          type="button"
          className="inline-flex items-center px-3 py-2 text-lg leading-4 font-medium rounded-md bg-transparent"
        >
          <ArrowLeftIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
          Back to requests
        </button>
      </div>
      <div className="h-full flex flex-col">
        <Tabs
          items={[
            {
              title: "Info",
              content: (
                <IncidentInfo
                  incident={currentlyViewedIncident}
                  dispatch={dispatch}
                  agents={agents}
                />
              ),
            },
            (currentlyViewedIncident && currentlyViewedIncident.status === 'started' && {
              title: "Agent",
              content: <PageLeftInFade key="child 2"><AgentAssigned /></PageLeftInFade>
            }),
          ]}
          defaultActiveIndex={0}
        />
      </div>
    </div>
  );
};

export default Incident;
