import React from "react";
import { useTable } from "react-table";
import PageLeftInFade from "modules/common/components/Animations/PageLeftInFade";
import Paginator from "components/Pagination/NoButtons";

const AgentCard = ({ id, agent, time, handleViewAgent }) => {
  return (
    <div className="flex items-center justify-between my-4">
      <div className="flex items-center">
        <img
          className="inline-block h-10 w-10 rounded-full mr-4"
          src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
          alt=""
        />
        <div className="flex flex-col">
          <span className="font-sans text-lg">{agent}</span>
          <button
            type="button"
            className="inline-flex self-start uppercase justify-center items-center px-2 py-1 border border-transparent text-sm font-medium rounded text-gray bg-blue-light hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            {time}
          </button>
        </div>
      </div>
      <div>
        <div
          className="flex items-center cursor-pointer"
          onClick={() => handleViewAgent(id)}
        >
          <button
            type="button"
            className="inline-flex uppercase items-center px-4 py-2 border-2 border-indigo text-sm font-medium rounded text-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            Assign
          </button>
        </div>
      </div>
    </div>
  );
};

const AvailableAgents = ({ agents, dispatch }) => {
  const handleViewAgent = (id) => {
    dispatch({ type: "viewAgent", payload: { id } });
  };
  const columns = React.useMemo(
    () => [
      {
        Header: "",
        accessor: (row) => row,
        id: "12",
        Cell: (props) => (
          <AgentCard
            agent={props.agent}
            time={props.time}
            id={props.id}
            handleViewAgent={props.handleViewAgent}
          />
        ),
      },
    ],
    []
  );

  const { rows, prepareRow } = useTable({
    columns,
    data: agents,
  });

  return (
    <div className="flex flex-col flex-grow">
      <PageLeftInFade key="child 1">
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <div className="border-b border-gray-lightest px-6">
              {row.cells.map((cell) => {
                return cell.render("Cell", { ...cell.value, handleViewAgent });
              })}
            </div>
          );
        })}
      </PageLeftInFade>
      <div className="mt-auto">
        <Paginator />
      </div>
    </div>
  );
};

AvailableAgents.defaultProps = {
  agents: [],
};

export default AvailableAgents;
