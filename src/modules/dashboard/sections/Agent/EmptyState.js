import React from "react";

const NoAgentAssigned = () => {
  return (
    <div className="h-full w-full px-32 flex items-center">
      <p className="text-gray-light text-left break-words font-sans">
        Assigning an agent to this incident. The agent’s profile will be shown
        here once they’re assigned.
      </p>
    </div>
  );
};

export default NoAgentAssigned;
