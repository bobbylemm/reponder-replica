import React from "react";
import PropTypes from "prop-types";
import AgentInfo from "./AgentInfo";
import NoAgentAssigned from "./EmptyState";

const AgentAssigned = () => {
  const data = null;
  return <>{data ? <AgentInfo data={data} /> : <NoAgentAssigned />}</>;
};

export default AgentAssigned;
