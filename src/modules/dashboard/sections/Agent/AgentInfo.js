import React from "react";
import PropTypes from 'prop-types'
import {
  ClockIcon,
  PencilIcon,
  PhoneIcon,
  MailIcon,
} from "@heroicons/react/solid";
import MedalIcon from "assets/icons/solid/medal.svg";
import CarIcon from "assets/icons/solid/car.svg";

const AgentInfo = ({data}) => {
  const agent = data?.agent ?? 'N/A'
  const time = data?.time ?? 'N/A'
  const stat = data?.stat ?? null

  return (
    <div className="p-6">
      <h2 className="text-2xl">{agent}</h2>
      <button
        type="button"
        className="inline-flex uppercase items-center px-2.5 my-2 py-1 border border-transparent text-sm font-medium rounded text-gray bg-blue-light hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        {time}
      </button>
      <p className="break-words pr-8 text-gray">
        Cluster C, 1004, Victoria Island, Laos state
      </p>

      <ul className="list-none space-y-4 mt-6 pt-6 border-t border-gray-lightest text-gray space-y-6">
        <li className="flex">
          <PhoneIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
          +2348090908987
        </li>
        <li className="flex">
          <MailIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
          email@email.com
        </li>
        <li className="flex">
          <MedalIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
          {stat?.completedTasks} completed tasks
        </li>
        <li className="flex">
          <CarIcon className="-ml-0.5 mr-2 h-6 w-6" aria-hidden="true" />
          {stat?.completionRate}% completed tasks
        </li>
      </ul>
      <div className="mt-6 py-6 border-t border-b border-gray-lightest flex">
        <span className="mr-4">Agent status</span>
        <button
          type="button"
          className="inline-flex uppercase items-center px-2.5 py-1 border border-transparent text-sm font-medium rounded text-gray bg-orange-light hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          busy
        </button>
      </div>
      <div className="mt-6">
        <button
          type="button"
          className="flex w-full justify-center items-center py-3 border border-transparent text-lg font-medium rounded text-white bg-indigo hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Replace agent
        </button>
      </div>
    </div>
  );
};

AgentInfo.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    time: PropTypes.string,
    state: PropTypes.shape({}),
  })
}

export default AgentInfo;
