import React, { useEffect } from "react";
import { useTable } from "react-table";
import path from "ramda/src/path";
import { ChevronRightIcon } from "@heroicons/react/outline";
import Paginator from "components/Pagination/NoButtons";
import useApiQuerySWR from "modules/common/hooks/useApiQuerySWR";
import configs from "configs/apiConfigs";
import Loader from "components/Loader";

const IncidentsCard = ({
  id,
  incidentType,
  victim,
  time,
  handleViewIncident,
}) => {
  return (
    <div className="flex items-center justify-between my-4">
      <div className="flex items-center">
        <img
          className="inline-block h-10 w-10 rounded-full mr-4"
          src="https://www.gravatar.com/avatar/12?d=retro"
          alt=""
        />
        <div className="flex flex-col">
          <span className="font-sans">{victim}</span>
          <span className="font-sans font-light text-sm">{time}</span>
        </div>
      </div>
      <div>
        <div
          className="flex items-center cursor-pointer"
          onClick={() => handleViewIncident(id)}
        >
          <button
            type="button"
            className="inline-flex uppercase items-center px-2.5 py-1 border border-transparent text-sm font-medium rounded text-gray bg-orange-light hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            pending
          </button>
          <ChevronRightIcon className="w-8 h-8 font-medium ml-4 text-gray-light" />
        </div>
      </div>
    </div>
  );
};

const INCIDENT_TYPE = "pending"

const PendingIncidents = ({ dispatch }) => {
  const { data, error, isValidating } = useApiQuerySWR(
    configs.services.DANGER_SERVICE_V1,
    `dangers?status=${INCIDENT_TYPE}`,
    { withAuthHeaders: true }
  );

  useEffect(() => {
    dispatch({
      type: "setIncidents",
      payload: { type: INCIDENT_TYPE, data: path(["data", "data"], data) || [] },
    });
  }, [data]);

  const handleViewIncident = (id) => {
    dispatch({ type: "viewIncident", payload: { id, type: INCIDENT_TYPE } });
  };
  const columns = React.useMemo(
    () => [
      {
        Header: "",
        accessor: (row) => row,
        id: "12",
        Cell: (props) => (
          <IncidentsCard
            incidentType={props.category}
            victim={`${props.user.first_name} ${props.user.last_name}` }
            time={"n/a"}
            id={props._id}
            handleViewIncident={props.handleViewIncident}
          />
        ),
      },
    ],
    []
  );

  const { rows, prepareRow } = useTable({
    columns,
    data: path(["data", "data"], data) || [],
  });

  return (
    <div className="flex flex-col flex-grow">
      {isValidating ? (
        <div className="w-full h-full flex justify-center items-center"><Loader /></div>
      ) : (
        rows.length ? rows.map((row, i) => {
          prepareRow(row);
          return (
            <div className="border-b border-gray-lightest px-6" key={i}>
              {row.cells.map((cell) => {
                return cell.render("Cell", {
                  ...cell.value,
                  handleViewIncident,
                });
              })}
            </div>
          );
        }) : <div>No data</div>
      )}
      <div className="mt-auto">
        <Paginator />
      </div>
    </div>
  );
};

PendingIncidents.defaultProps = {
  incidents: [],
};

export default PendingIncidents;
