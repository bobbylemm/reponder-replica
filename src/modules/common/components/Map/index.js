import React from "react";
import GoogleMapReact from "google-map-react";
import isEmpty from "ramda/src/isEmpty";
import map from "ramda/src/map";
import { LocationMarkerIcon } from "@heroicons/react/solid";

import aubergine from "./configs/aubergine";

const { NEXT_PUBLIC_GOOGLE_MAP_API_KEY } = process.env;

const Marker = ({ lat, lng }) => {
  return (
    <>
      <LocationMarkerIcon className="h-16 w-16 text-yellow" />
      {/* {show && <InfoWindow place={place} />} */}
    </>
  );
};

const Map = ({ focusOnCoordinate, markersCoordinates }) => {
  return (
    <div
      className="row"
      style={{
        height: "100%",
      }}
    >
      <GoogleMapReact
        bootstrapURLKeys={{ key: NEXT_PUBLIC_GOOGLE_MAP_API_KEY }}
        defaultCenter={{
          lat: 6.5244,
          lng: 3.3792,
        }}
        defaultZoom={11}
        zoom={12}
        options={{
          styles: aubergine,
          zoomControl: false,
        }}
        center={
          !isEmpty(focusOnCoordinate)
            ? {
                lat: focusOnCoordinate.latitude,
                lng: focusOnCoordinate.longitude,
              }
            : !isEmpty(markersCoordinates)
            ? {
                lat: markersCoordinates[0].latitude,
                lng: markersCoordinates[0].longitude,
              }
            : {}
        }
      >
        {map(
          (location, index) => (
            <Marker
              lat={location.latitude}
              lng={location.longitude}
              key={index}
            />
          ),
          markersCoordinates
        )}
      </GoogleMapReact>
    </div>
  );
};

Map.defaultProps = {
  focusOnCoordinate: {},
  markersCoordinates: [],
};

export default Map;
