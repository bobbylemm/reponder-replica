import React, {useCallback, useState, useEffect} from 'react'
import PropTypes from 'prop-types'
import {useRouter} from 'next/router'

import DashboardNavbar from 'modules/common/components/DashboardNavbar'
import NarrowSidebar from 'modules/common/components/Sidebars/NarrowSidebar'
import MobileSidebar from 'modules/common/components/Sidebars/MobileSidebar'
// import DashboardFooter from 'modules/common/components/DashboardFooter'

const DashboardLayout = ({children}) => {
  const router = useRouter()
  const [isMobileMenuOpen, setMobileMenuOpen] = useState(false)
  const closeMobileMenu = useCallback(() => setMobileMenuOpen(false), [])
  const openMobileMenu = useCallback(() => setMobileMenuOpen(true), [])

  useEffect(() => {
    router.events.on('routeChangeStart', closeMobileMenu)
    return () => {
      router.events.off('routeChangeStart', closeMobileMenu)
    }
  }, [closeMobileMenu, router])

  return (
    <div aria-label="DashboardLayout">
      <div className="relative h-screen">
        <div className="flex overflow-hidden">
          {/* Narrow sidebar */}
          <NarrowSidebar />

          {/* Mobile menu */}
          <MobileSidebar
            isMenuOpen={isMobileMenuOpen}
            onClose={closeMobileMenu}
          />

          {/* Content area */}
          <div className="flex flex-col flex-1 overflow-hidden">
            {isMobileMenuOpen && <DashboardNavbar onOpen={openMobileMenu} />}

            {/* Main content */}
            <div className="flex-1 overflow-hidden">
              <main className="overflow-y-auto h-full">{children}</main>
            </div>
          </div>
        </div>
        {/* <DashboardFooter /> */}
      </div>
    </div>
  )
}

DashboardLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default DashboardLayout
