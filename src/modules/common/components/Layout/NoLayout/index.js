import React from 'react'
import PropTypes from 'prop-types'

const NoLayout = ({children}) => {
  return (
    <div className="relative">
      {children}
    </div>
  )
}

NoLayout.propTypes = {
  children: PropTypes.node,
}

export default NoLayout
