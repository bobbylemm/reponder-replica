import dynamic from 'next/dynamic'

export const LAYOUT_TYPE = {
  none: 'none',
  dashboard: 'dashboard',
}

export const LAYOUT_MAP = {
  [LAYOUT_TYPE.none]: {
    component: dynamic(() => import('./NoLayout')),
    props: [],
  },
  [LAYOUT_TYPE.dashboard]: {
    component: dynamic(() => import('./DashboardLayout')),
    props: [],
  },
}

