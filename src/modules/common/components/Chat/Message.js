import React from "react";
import classNames from "classnames";
import { motion } from "framer-motion";
import {messageVariants} from './variants/message'

const Message = ({ message }) => {
  return (
    <motion.div
      variants={messageVariants}
      className={classNames(
        { "justify-end": message.sentBy === "agent" },
        "flex items-start my-2"
      )}
    >
      <div className="flex flex-col space-y-2 text-xs max-w-xs mx-2 order-1 items-end">
        <span
          className={classNames(
            {
              "border-gray-light bg-white border rounded-br-none": message.sentBy === "agent",
              "bg-indigo-alpha1 rounded-tl-none": message.sentBy === "user",
            },
            "px-4 rounded-lg inline-block text-black font-sans3 py-3"
          )}
        >
          {message.text}
        </span>
      </div>
      {message.sentBy === "user" ? (
        <img
          className="h-4 w-4 rounded-full"
          src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
          alt=""
        />
      ) : null}
    </motion.div>
  );
};

export default Message;
