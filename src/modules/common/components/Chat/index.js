import React, { useState, useRef, useEffect } from "react";
import PropTypes from 'prop-types'
import { motion } from "framer-motion";
import { EmojiHappyIcon } from "@heroicons/react/outline";
import { LocationMarkerIcon } from "@heroicons/react/solid";
import Message from "./Message";
import {chatCardVariants} from './variants/chatCard'
import SendIcon from "assets/icons/solid/sendIcon.svg";

const Input = ({ setIsOpen }) => {
  return (
    <div className="relative rounded-md shadow-sm">
      <input
        type="text"
        name="message"
        id="message"
        className="focus:ring-0 focus:border-gray-lightest block w-full pr-10 border-t border-l-0 border-r-0 border-b-0 border-gray-lightest"
        placeholder="type something"
        onClick={() => setIsOpen((s) => !s)}
        onBlur={() => setIsOpen((s) => !s)}
      />
      <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
        <EmojiHappyIcon className="h-5 w-5 text-gray mr-2" aria-hidden="true" />
        <SendIcon className="h-8 w-8" aria-hidden="true" />
      </div>
    </div>
  );
};

const ChatCard = ({messages}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [client, setClient] = useState(null);
  const clientRef = useRef(null);
  clientRef.current = client;

  const [channel, setChannel] = useState(null);
  const channelRef = useRef(null);
  channelRef.current = channel;

  const [message, setMessage] = useState("");
  const messageRef = useRef("");
  messageRef.current = message;

  const [messageData, setMessageData] = useState([]);
  const messageDataRef = useRef([]);
  messageDataRef.current = messageData;

  useEffect(() => {
    initializeClient();
    createChannel();
  }, []);

  const initializeClient = async () => {};
  const createChannel = async () => {
    setMessageData(messages);
  };

  const getMessageHandler = (e) => {
    setMessage(e.target.value);
  };

  const sendMessageHandler = async (e) => {
    if (e.charCode === 13) {
      if (channelRef.current) {
        if (messageRef.current.trim().length > 0) {
          const message = await channelRef.current.sendMessage({
            text: messageRef.current,
          });
          setMessage("");
        }
      }
    }
  };
  return (
    <div className="bg-white w-full rounded-md overflow-auto">
      <div className="flex space-x-3 bg-white-dark py-4 px-2">
        <div className="flex-shrink-0">
          <img
            className="h-10 w-10 rounded-full"
            src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt=""
          />
        </div>
        <div className="min-w-0 flex-1">
          <p className="text-base font-medium text-gray-900">
            <a href="#" className="hover:underline">
              Message Alena press
            </a>
          </p>
          <div className="flex items-center">
            <LocationMarkerIcon className="h-4 w-4 text-indigo-lighter" />
            <p className="text-sm text-gray-light">
              <a href="#" className="hover:underline">
                Cluster C, 1004, Victoria Island, Laos state
              </a>
            </p>
          </div>
        </div>
      </div>
      <motion.div
        initial={"closed"}
        animate={isOpen ? "open" : "closed"}
        // @ts-ignore
        variants={chatCardVariants}
        className="p-2 overflow-auto"
      >
        {Array.from(messages).map((message) => (
          <Message key={message.id} message={message} />
        ))}
      </motion.div>
      <Input setIsOpen={setIsOpen} />
    </div>
  );
};

ChatCard.defaultProps = {
  messages: []
}

ChatCard.propTypes = {
  messages: PropTypes.array
}

export default ChatCard;
