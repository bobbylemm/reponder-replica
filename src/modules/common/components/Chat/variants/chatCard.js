export const chatCardVariants = {
  open: {
    height: "12rem",
    display: "block",
    visibility: "visible",
    transition: {
      staggerChildren: 0.17,
      delayChildren: 0.2,
    },
  },
  closed: {
    height: "0",
    display: "none",
    transition: {
      staggerChildren: 0.05,
      staggerDirection: -1,
      when: "afterChildren",
    },
  },
};
