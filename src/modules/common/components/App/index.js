import React, {useEffect, useMemo} from 'react'
import PropTypes from 'prop-types'
import {useRouter} from 'next/router'
import pick from 'lodash/pick'

import Loader from 'components/Loader'
import {LAYOUT_MAP} from 'modules/common/components/Layout/config'
import {PAGE_PATHS} from 'modules/common/const'
import { useAuthContext } from 'modules/auth/context/auth'

const App = ({Component, pageProps, err}) => {
  const router = useRouter()
  const {data} = useAuthContext()
  const layoutConfig = useMemo(
    () => LAYOUT_MAP[pageProps.layout?.type] || LAYOUT_MAP.none,
    [pageProps.layout]
  )
  const layoutProps = useMemo(
    () => pick(pageProps.layout, layoutConfig.props),
    [pageProps.layout, layoutConfig]
  )
  const Layout = layoutConfig.component

  // if the page is protected and user is not logged in,
  // redirect to login page
  useEffect(() => {
    if (!pageProps.isProtected) return

    if (!data.data.loggedIn) {
      router.replace({
        pathname: PAGE_PATHS.LOGIN,
        query: {},
      })
    }
  }, [pageProps.isProtected, router])

  // return a loading screen for protected pages with no logged in user
  if (pageProps.isProtected) {
    return (
      <>
        <div className="flex items-center justify-center h-screen bg-beige">
          <Loader />
        </div>
      </>
    )
  }

  return (
    <>
      {/** @ts-ignore */}
      <Layout {...layoutProps}>
        <Component {...pageProps} err={err} />
      </Layout>
    </>
  )
}

App.propTypes = {}

export default App
