import React from "react";
import classNames from "classnames";
import toast from "react-hot-toast";
import { BanIcon, XIcon } from "@heroicons/react/solid";

/**
 * Function to create notification
 *
 * @example
 * const {error, data, isLoading, action} = notify(
 *    {
 *      "type": "error",
 *      "message": "error occurred"
 *    }
 * )
 *
 * @param {Input} options The backend service to be called
 */

export const notify = ({ type, message }) =>
  toast.custom((t) => (
    <div
      className={classNames(
        {
          "animate-enter": t.visible,
          "animate-leave": !t.visible,
          "bg-white": !type,
          "bg-orange-light": type === "error",
          "bg-green": type === "success",
        },
        "max-w-md w-full  shadow-lg rounded-lg pointer-events-auto flex ring-1 ring-black ring-opacity-5"
      )}
    >
      <div className="flex-1 w-0 p-4">
        <div className="flex items-center">
          <div className="flex-shrink-0 pt-0.5">
            {type === "error" && <BanIcon className="h-12 w-12" />}
          </div>
          <div className="ml-3 flex-1">
            <p className="text-sm font-medium text-gray-900">{message}</p>
          </div>
        </div>
      </div>
      <div className="flex border-l border-gray-lightest">
        <button
          onClick={() => toast.dismiss(t.id)}
          className="w-full border border-transparent rounded-none rounded-r-lg p-4 flex items-center justify-center text-sm font-medium text-indigo-600 hover:text-indigo-500 focus:outline-none"
        >
          <XIcon className="h-8 w-8" />
        </button>
      </div>
    </div>
  ));

/**
 * @typedef {Object} Input
 * @property {('error'|'success')} type The type of notification to display
 * @property {string} message The notification message to display
 */
