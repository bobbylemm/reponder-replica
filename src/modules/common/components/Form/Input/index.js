import React, { useMemo } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import useFormField from "modules/common/components/Form/hooks/useFormField";
import { ExclamationCircleIcon as ErrorIcon } from "@heroicons/react/solid";

import InputFeedback from "../InputFeedback";

const Input = (props) => {
  const {
    label,
    placeholder,
    labelClassName,
    fieldClassName,
    type,
    ...otherProps
  } = props;

  const {
    hasError: formikHasError,
    field: { name, onChange, onBlur },
    formContext: { errors, dirty },
    meta: { touched },
  } = useFormField(otherProps);

  const hasError = useMemo(
    () => touched && formikHasError && dirty,
    [formikHasError, touched, dirty]
  );

  return (
    <div>
      <label htmlFor={name} className="block text-sm font-thin text-gray-200">
        {label}
      </label>
      <div
        className={classNames(
          "mt-1 relative",
          labelClassName
        )}
      >
        <input
          type={type}
          name={name}
          id={name}
          className={classNames(
            "appearance-none block w-full px-3 py-4 border border-gray-lightest rounded-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",
            hasError
              ? "border-red text-red placeholder-red focus:outline-none focus:ring-red focus:border-red"
              : "",
            fieldClassName
          )}
          placeholder={placeholder}
          aria-invalid="true"
          aria-describedby={`${name}-error`}
          onChange={onChange}
          onBlur={onBlur}
        />
        {hasError && (
          <div className="absolute inset-y-0 right-0 flex items-center pr-3 pointer-events-none">
            <div className="w-5 h-5 text-red-dark" aria-hidden="true">
              <ErrorIcon />
            </div>
          </div>
        )}
      </div>
      {hasError && (
        <InputFeedback
          // @ts-ignore
          error={errors[name]}
          name={name}
        />
      )}
    </div>
  );
};

Input.defaultProps = {
  type: "text",
};

Input.propTypes = {
  fieldClassName: PropTypes.string,
  label: PropTypes.string,
  labelClassName: PropTypes.string,
  name: PropTypes.any,
  placeholder: PropTypes.string,
  type: PropTypes.string,
};

export default Input;
