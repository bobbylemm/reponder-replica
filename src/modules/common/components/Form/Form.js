import React from 'react'
import PropTypes from 'prop-types'
import {Formik, Form as FormikForm} from 'formik'

const Form = ({
  initialValues,
  validationSchema,
  onSubmit,
  children,
  ...props
}) => (
  <Formik
    validateOnMount
    validateOnBlur
    validateOnChange
    {...props}
    initialValues={initialValues}
    validationSchema={validationSchema}
    onSubmit={onSubmit}>
    <FormikForm>{children}</FormikForm>
  </Formik>
)

Form.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  initialValues: PropTypes.object.isRequired,
  onSubmit: PropTypes.func,
  validationSchema: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
}

export default Form
