import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

const InputFeedback = ({error, className, name}) =>
  error ? (
    <p
      className={classNames('mt-2 text-sm text-red', className)}
      id={`${name}-error`}
      data-testid={name}>
      {error}
    </p>
  ) : null

InputFeedback.propTypes = {
  className: PropTypes.string,
  error: PropTypes.string,
  name: PropTypes.string,
}

export default InputFeedback
