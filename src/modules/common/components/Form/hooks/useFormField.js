import {useMemo} from 'react'
import {useField, useFormikContext} from 'formik'

/**
 * @param {FormFieldHookConfig} input
 * @returns {useFormFieldResponse}
 */

export default function useFormField({disabled: isDisabled, id, ...props}) {
  const [field, meta, helpers] = useField(props)
  const formContext = useFormikContext()

  const {touched, error} = meta
  const hasError = !!(touched && error)

  return useMemo(
    () => ({
      field,
      meta,
      helpers,
      formContext,
      hasError,
      isDisabled,
      id,
    }),
    [field, formContext, hasError, helpers, id, isDisabled, meta]
  )
}

/**
 * @typedef {import('formik').FieldHookConfig<any> & {disabled?: boolean, id?: string}} FormFieldHookConfig
 */

/**
 * @typedef {object} useFormFieldResponse
 * @property {boolean} hasError
 * @property {boolean} isDisabled
 * @property {string} id
 * @property {import('formik').FieldInputProps<any>} field
 * @property {import('formik').FormikContextType<any>} formContext
 * @property {import('formik').FieldMetaProps<any>} meta
 * @property {import('formik').FieldHelperProps<any>} helpers
 */
