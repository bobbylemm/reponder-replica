import React from "react";
import { motion } from "framer-motion";

const PageLeftInFade = ({ key, children }) => {
  return (
    <motion.div
      key={key}
      initial={{ x: -50, opacity: 0 }}
      animate={{ x: 0, opacity: 1, transitionDuration: "2ms" }}
      exit={{ x: -50, opacity: 0 }}
      className="h-full"
    >
      {children}
    </motion.div>
  );
};

export default PageLeftInFade;
