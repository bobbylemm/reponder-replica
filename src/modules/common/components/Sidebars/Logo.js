import React from 'react'
import Image from 'next/image'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Link from 'next/link'

import {PAGE_PATHS} from 'modules/common/const'

const Logo = ({className}) => {
  return (
    <Link href={PAGE_PATHS.INCIDENTS} passHref>
        <div
            className={classNames(
                'relative flex items-center flex-shrink-0 w-12 h-8',
                className
            )}>
            <Image
                src="/img/logo-dark.png"
                layout="fill"
                objectFit="contain"
                alt="Sety Logo"
            />
        </div>
    </Link>
  )
}

Logo.propTypes = {
  className: PropTypes.string,
}

export default Logo
