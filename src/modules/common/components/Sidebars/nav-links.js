import {CheckIcon} from '@heroicons/react/outline'
import Indicator from 'assets/icons/solid/indicator.svg'
import Settings from 'assets/icons/outline/settings.svg'
import {PAGE_PATHS} from 'modules/common/const'

export default [
  {
    title: 'Incidents',
    href: PAGE_PATHS.INCIDENTS,
    IconComponent: Indicator,
  },
  {
    title: 'Completed',
    href: PAGE_PATHS.COMPLETED,
    IconComponent: CheckIcon,
  },
  {
    title: 'Settings',
    href: PAGE_PATHS.SETTINGS,
    IconComponent: Settings,
  },
]
