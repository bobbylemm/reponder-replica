import React, { Fragment } from "react";
import { Dialog, Transition } from "@headlessui/react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Link from "next/link";
import { useRouter } from "next/router";

import { XIcon } from '@heroicons/react/outline'
import LINKS from "../nav-links";
import Logo from "../Logo";

const MobileSidebar = ({ isMenuOpen, onClose }) => {
  const router = useRouter();

  return (
    <Transition.Root show={isMenuOpen} as={Fragment}>
      <Dialog
        as="div"
        static
        className="md:hidden"
        open={isMenuOpen}
        onClose={onClose}
      >
        <div className="fixed inset-0 z-40 flex">
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-opacity-75 bg-gray" />
          </Transition.Child>
          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="relative flex flex-col flex-1 w-full max-w-xs pt-5 pb-4 bg-indigo">
              <Transition.Child
                as={Fragment}
                enter="ease-in-out duration-300"
                enterFrom="opacity-0"
                enterTo="opacity-100"
                leave="ease-in-out duration-300"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <div className="absolute right-0 p-1 top-1 -mr-14">
                  <button
                    type="button"
                    className="flex items-center justify-center w-12 h-12 focus:outline-none"
                    onClick={onClose}
                  >
                    <XIcon className="w-6 h-6 text-black-light" aria-hidden="true" />
                    <span className="sr-only">Close sidebar</span>
                  </button>
                </div>
              </Transition.Child>
              <Logo className="px-4 ml-3" />
              <div className="flex-1 h-0 px-2 mt-5 overflow-y-auto">
                <nav className="flex flex-col h-full">
                  <div className="mb-auto space-y-1">
                    {LINKS.map(({ title, href, IconComponent }) => {
                      const active = router.pathname === href;
                      return (
                        <Link
                          key={title}
                          href={href}
                          passHref
                          aria-current={active ? "page" : undefined}
                        >
                          <div
                            className={classNames(
                              "group py-2 px-3 rounded-md flex items-center text-sm font-medium hover:bg-indigo",
                              {
                                "bg-indigo-dark text-white": active,
                                "text-gray-dark hover:text-white": !active,
                              }
                            )}
                          >
                            <IconComponent
                              className={classNames("mr-3 h-6 w-6", {
                                "text-white": active,
                                "text-gray-dark group-hover:text-white":
                                  !active,
                              })}
                              aria-hidden="true"
                            />
                            <span>{title}</span>
                          </div>
                        </Link>
                      );
                    })}
                  </div>
                </nav>
              </div>
            </div>
          </Transition.Child>
          <div className="flex-shrink-0 w-14" aria-hidden="true">
            {/* Dummy element to force sidebar to shrink to fit close icon */}
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

MobileSidebar.propTypes = {
  isMenuOpen: PropTypes.bool,
  onClose: PropTypes.func,
};

export default MobileSidebar;
