import React from "react";
import classNames from "classnames";
import Link from "next/link";
import { useRouter } from "next/router";
import LINKS from "../nav-links";
import Logo from "assets/svg/logo-dark.svg";

const NarrowSidebar = () => {
  const router = useRouter();

  return (
    <div className="hidden h-screen overflow-y-auto bg-indigo-lightest w-1/6 md:block">
      <div className="flex flex-col items-start w-full py-6">
        <div className="px-8">
          <Logo />
        </div>
        <div className="flex-1 w-full px-2 mt-6 space-y-8 border-t border-white pt-10">
          {LINKS.map(({ title, href, IconComponent }) => {
            const active = router.pathname === href;
            return (
              <Link
                key={title}
                href={href}
                aria-current={active ? "page" : undefined}
              >
                <div className="flex items-center px-6">
                  <IconComponent
                      className={classNames('h-6 w-6', {
                        'text-indigo': active,
                        'text-purple-light group-hover:text-beige-dark': !active,
                      })}
                      aria-hidden="true"
                    />
                  <span className={classNames({'text-indigo': active, 'text-gray-darker': !active}, "ml-4")}>{title}</span>
                </div>
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
};

NarrowSidebar.propTypes = {};

export default NarrowSidebar;
