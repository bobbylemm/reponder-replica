import {useState, useMemo, useCallback} from 'react'

export default function useModal(defaultIsOpen = false) {
  const [isOpen, setIsOpen] = useState(defaultIsOpen)
  const open = useCallback(() => setIsOpen(true), [])
  const close = useCallback(() => setIsOpen(false), [])
  const toggle = useCallback(() => setIsOpen((isOpen) => !isOpen), [])

  return useMemo(() => ({isOpen, open, close, toggle}), [
    isOpen,
    open,
    close,
    toggle,
  ])
}
