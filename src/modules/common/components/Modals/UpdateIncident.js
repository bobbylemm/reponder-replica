import React, { Fragment, useState, useMemo, useEffect } from "react";
import classNames from "classnames";
import map from "ramda/src/map";
import prop from "ramda/src/prop";
import propEq from "ramda/src/propEq";
import find from "ramda/src/find";
import defaultTo from "ramda/src/defaultTo";
import isEmpty from "ramda/src/isEmpty";
import { Dialog, Transition, Listbox } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/solid";
import Button from "modules/common/components/Button";
import ChevronDownThick from "assets/svg/ChevronDownThick.svg";
import ModalCloseButton from "assets/svg/modalCloseButton.svg";
import useApiQuery from "modules/common/hooks/useApiQuery";
import configs from "configs/apiConfigs";
import useApiMutation from "modules/common/hooks/useApiMutation";
import { notify } from "../Notification";

const Select = ({ title, data, setSelected, selected }) => {
  const getLabel = propEq("id", selected);
  const selectedLabel = selected ? prop("label", find(getLabel)(data)) : null;

  return (
    <Listbox value={selected} onChange={setSelected}>
      <div className="mt-1 relative">
        <Listbox.Button className="bg-white relative w-full border-r-0 border-l-0 border-t-0 border-b border-gray-dark pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
          <span className="block truncate text-gray-dark text-base">
            {selectedLabel ?? title}
          </span>
          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <ChevronDownThick
              className="h-3 w-3 text-gray-light"
              aria-hidden="true"
            />
          </span>
        </Listbox.Button>

        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Listbox.Options className="absolute z-10 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
            {!isEmpty(data) ? (
              data.map((option) => (
                <Listbox.Option
                  key={option.id}
                  className={({ active }) =>
                    classNames(
                      { "text-white bg-indigo": active, "text-gray": !active },
                      "cursor-default select-none relative py-3 pl-6 pr-9"
                    )
                  }
                  value={option.id}
                >
                  {({ selected, active }) => (
                    <>
                      {React.cloneElement(option.component, {
                        className: classNames(
                          {
                            "font-semibold": selected,
                            "font-normal": !selected,
                          },
                          option.component.props.className,
                          "truncate"
                        ),
                      })}
                      {selected ? (
                        <span
                          className={classNames(
                            { "text-white": active, "text-indigo": !active },
                            "absolute inset-y-0 right-0 flex items-center pr-4"
                          )}
                        >
                          <CheckIcon className="h-5 w-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))
            ) : (
              <div>No data</div>
            )}
          </Listbox.Options>
        </Transition>
        <Listbox.Label className="block font-medium text-gray-dark mt-2 text-xs lowercase">
          Select the {title} to update the incident
        </Listbox.Label>
      </div>
    </Listbox>
  );
};

const UpdateIncident = ({ open, setOpen, incidentId }) => {
  const [categorySelected, setCategorySelected] = useState(null);
  const [statusSelected, setStatusSelected] = useState(null);

  const {
    data = {},
    error: statError,
    isLoading,
    action: getCategories,
    reset: getCategoriesReset,
  } = useApiQuery(
    configs.services.DANGER_SERVICE_V1,
    "categories?active=true",
    { withAuthHeaders: true }
  );

  const {
    action: updateIncidentCategory,
    isLoading: updateIncidentCategoryLoading,
    error: updateIncidentCategoryError,
    reset: updateIncidentCategoryReset,
    data: updateIncidentCategoryData,
  } = useApiMutation(
    configs.services.DANGER_SERVICE_V1,
    `dangers/${incidentId}/update-category`,
    "post",
    { withAuthHeaders: true }
  );

  const {
    action: updateIncidentStatus,
    isLoading: updateIncidentStatusLoading,
    error: updateIncidentStatusError,
    reset: updateIncidentStatusReset,
    data: updateIncidentStatusData,
  } = useApiMutation(
    configs.services.DANGER_SERVICE_V1,
    `dangers/${incidentId}/update-status`,
    "post",
    { withAuthHeaders: true }
  );

  useEffect(() => {
    (async () => {
      await getCategories();
    })();
  }, []);

  useEffect(() => {
    if (updateIncidentStatusError || updateIncidentCategoryError) {
      notify({ type: "error", message: "Error updating incident" });
    }

    if (updateIncidentStatusData && updateIncidentCategoryData) {
      notify({type: "success", message: "Successfully updated incident"})
      setOpen(false);
      updateIncidentStatusReset();
      updateIncidentCategoryReset();
    }
  }, [
    updateIncidentStatusError,
    updateIncidentCategoryError,
    updateIncidentStatusData,
    updateIncidentCategoryData,
  ]);

  const handleUpdate = async () => {
    await Promise.all([
      updateIncidentCategory({ category: categorySelected }),
      updateIncidentStatus({ status: statusSelected }),
    ]);
  };

  const categoriesToSelect = useMemo(() => {
    return map((category) => {
      return {
        id: category?._id,
        label: category?.name,
        component: (
          <div className="flex flex-col">
            <span className="text-base text-black">{category?.name}</span>
          </div>
        ),
      };
    }, defaultTo([])(prop("data", data)));
  }, [data]);

  const statusToSelect = useMemo(() => {
    return map(
      (category) => {
        return {
          id: category,
          label: category,
          component: (
            <div className="flex flex-col">
              <span className="text-base text-black">{category}</span>
            </div>
          ),
        };
      },
      ["pending", "started", "abandoned", "completed"]
    );
  }, []);

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed z-10 inset-0 overflow-y-auto"
        onClose={setOpen}
      >
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white rounded-md pt-5 pb-4 text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full relative">
              <div className="hidden sm:block absolute -top-8 -right-8 pt-4 pr-4 z-20">
                <ModalCloseButton onClick={() => setOpen(false)} />
              </div>
              <div className="">
                <div className="mt-3 text-center sm:mt-0 sm:text-left">
                  <Dialog.Title
                    as="h3"
                    className="text-lg leading-6 text-gray-darker font-medium border-b border-gray-light p-6 px-8 pb-8"
                  >
                    Update Incident
                  </Dialog.Title>
                  <div className="p-6 px-8">
                    <Select
                      title="Incident type"
                      data={categoriesToSelect}
                      setSelected={setCategorySelected}
                      selected={categorySelected}
                    />
                    <Select
                      title="Status"
                      data={statusToSelect}
                      setSelected={setStatusSelected}
                      selected={statusSelected}
                    />
                  </div>
                </div>
              </div>
              <div className="flex flex-row border-t border-gray-light p-6 items-center justify-end w-full">
                <button
                  type="button"
                  className="w-full inline-flex justify-center bg-transparent px-8 py-3 text-base rounded-sm font-medium hover:bg-gray-lightest focus:outline-none sm:mr-3 sm:w-auto sm:text-sm"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
                <Button
                  label="Update Incident"
                  loading={
                    updateIncidentCategoryLoading || updateIncidentStatusLoading
                  }
                  type="button"
                  className="mt-3 w-full inline-flex justify-center shadow-sm px-8 py-3 rounded-sm bg-indigo text-base font-medium text-white hover:text-gray-lightest focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm"
                  onClick={handleUpdate}
                />
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default UpdateIncident;
