import React, {Fragment, cloneElement, useRef} from 'react'
import {Dialog, Transition} from '@headlessui/react'
import PropTypes from 'prop-types'
import always from 'ramda/src/always'

export default function Modal({
  modalState,
  children,
  title,
  icon,
  dismissible,
  content,
  actionButton,
  cancelButton,
}) {
  const divRef = useRef(null)

  const actionBtn = actionButton
    ? cloneElement(actionButton, {
        onClick: (e) => {
          if (
            actionButton.props &&
            typeof actionButton.props.onClick === 'function'
          ) {
            actionButton.props.onClick(e)
          }
          modalState.close()
        },
      })
    : null
  const cancelBtn = cancelButton
    ? cloneElement(cancelButton, {
        onClick: (e) => {
          if (
            cancelButton.props &&
            typeof cancelButton.props.onClick === 'function'
          ) {
            cancelButton.props.onClick(e)
          }
          modalState.close()
        },
      })
    : null

  return (
    <Transition.Root show={true}>
      <Dialog
        as="div"
        static
        initialFocus={divRef}
        className="fixed inset-0 z-50 overflow-y-auto"
        open={modalState.isOpen}
        onClose={dismissible ? modalState.close : always(undefined)}>
        <div className="flex items-end justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0">
            <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray bg-opacity-75" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95">
            {children || (
              <div className="inline-block px-4 pt-5 pb-4 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6">
                <div>
                  {icon && (
                    <div className="flex items-center justify-center w-12 h-12 mx-auto bg-green-100 rounded-full">
                      {icon}
                    </div>
                  )}
                  <div className="mt-2 text-center sm:mt-5">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-gray-dark">
                      {title}
                    </Dialog.Title>
                    <div className="mt-2 text-sm">{content}</div>
                  </div>
                </div>
                <div className="mt-5 sm:mt-6" ref={divRef}>
                  {actionBtn}
                  {cancelBtn}
                </div>
              </div>
            )}
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}

Modal.defaultProps = {
  dismissible: true,
}

Modal.propTypes = {
  actionButton: PropTypes.node,
  cancelButton: PropTypes.node,
  children: PropTypes.node,
  content: PropTypes.node,
  dismissible: PropTypes.bool,
  icon: PropTypes.node,
  modalState: PropTypes.shape({
    open: PropTypes.func.isRequired,
    close: PropTypes.func.isRequired,
    isOpen: PropTypes.bool,
    toggle: PropTypes.func.isRequired,
  }).isRequired,
  title: PropTypes.node,
}
