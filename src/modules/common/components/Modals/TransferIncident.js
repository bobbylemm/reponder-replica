import React, { Fragment, useState, useMemo } from "react";
import classNames from "classnames";
import { map } from "ramda";
import { Dialog, Transition, Listbox } from "@headlessui/react";
import { CheckIcon } from "@heroicons/react/solid";
import ChevronDownThick from "assets/svg/ChevronDownThick.svg";
import ModalCloseButton from "assets/svg/modalCloseButton.svg";

const Select = ({ data }) => {
  const [selected, setSelected] = useState(data[0]);

  return (
    <Listbox value={selected} onChange={setSelected}>
      <div className="mt-1 relative">
        <Listbox.Button className="bg-white relative w-full border-r-0 border-l-0 border-t-0 border-b border-gray-dark pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
          <span className="block truncate text-gray-dark text-base">
            Select agent
          </span>
          <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
            <ChevronDownThick
              className="h-3 w-3 text-gray-light"
              aria-hidden="true"
            />
          </span>
        </Listbox.Button>

        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <Listbox.Options className="absolute z-10 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
            {data.map((option) => (
              <Listbox.Option
                key={option.id}
                className={({ active }) =>
                  classNames(
                    { "text-white bg-indigo": active, "text-gray": !active },
                    "cursor-default select-none relative py-3 pl-6 pr-9"
                  )
                }
                value={option.label}
              >
                {({ selected, active }) => (
                  <>
                    {React.cloneElement(option.component, {
                      className: classNames(
                        { "font-semibold": selected, "font-normal": !selected },
                        option.component.props.className,
                        "truncate"
                      ),
                    })}
                    {selected ? (
                      <span
                        className={classNames(
                          { "text-white": active, "text-indigo": !active },
                          "absolute inset-y-0 right-0 flex items-center pr-4"
                        )}
                      >
                        <CheckIcon className="h-5 w-5" aria-hidden="true" />
                      </span>
                    ) : null}
                  </>
                )}
              </Listbox.Option>
            ))}
          </Listbox.Options>
        </Transition>
        <Listbox.Label className="block font-medium text-gray-dark mt-2 text-xs">
          Select the agent you want to transfer this request to
        </Listbox.Label>
      </div>
    </Listbox>
  );
};

const TransferIncident = ({ open, setOpen, agents }) => {
  const agentsToSelect = useMemo(() => {
    return map((agent) => {
      return {
        id: agent?.id,
        label: agent.agent,
        component: (
          <div className="flex flex-col">
            <span className="text-base text-black">{agent.agent}</span>
            <span className="text-sm text-gray-light">{agent.email}</span>
          </div>
        ),
      };
    }, agents);
  }, [agents]);
  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="fixed z-10 inset-0 overflow-y-auto"
        onClose={setOpen}
      >
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="hidden sm:inline-block sm:align-middle sm:h-screen"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white rounded-md pt-5 pb-4 text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full relative">
              <div className="hidden sm:block absolute -top-8 -right-8 pt-4 pr-4 z-20">
                <ModalCloseButton onClick={() => setOpen(false)} />
              </div>
              <div className="">
                <div className="mt-3 text-center sm:mt-0 sm:text-left">
                  <Dialog.Title
                    as="h3"
                    className="text-lg leading-6 text-gray-darker font-medium border-b border-gray-light p-6 px-8 pb-8"
                  >
                    Transfer Request
                  </Dialog.Title>
                  <div className="p-6 px-8">
                    <p className="text-gray-dark text-md">
                      You are about to transfer this request to another
                      responder agent
                    </p>
                    <p className="text-gray-dark text-md my-5">
                      Select agent to transfer request to:
                    </p>
                    <Select data={agentsToSelect} />
                  </div>
                </div>
              </div>
              <div className="flex flex-row border-t border-gray-light p-6 items-center justify-end w-full">
                <button
                  type="button"
                  className="w-full inline-flex justify-center bg-transparent px-8 py-3 text-base rounded-sm font-medium hover:bg-gray-lightest focus:outline-none sm:mr-3 sm:w-auto sm:text-sm"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center shadow-sm px-8 py-3 rounded-sm bg-indigo text-base font-medium text-white hover:text-gray-lightest focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm"
                  onClick={() => setOpen(false)}
                >
                  Transfer request
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default TransferIncident;
