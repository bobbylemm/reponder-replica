import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Loader from 'components/Loader'

const Button = (props) => {
  const {
    className,
    label,
    disabled,
    loading,
    ...otherProps
  } = props

  return (
    <button
      {...otherProps}
      disabled={disabled}
      className={classNames(
        'flex items-center justify-center px-4 py-1.5 text-base font-medium md:text-base md:px-5 btn-primary w-full',
        {'opacity-75 cursor-default': disabled},
        className
      )}>
      {loading ? (
        <Loader
          sizeClassName="w-4 h-4 sm:w-6 sm:h-6 lg:w-6 lg:h-6"
          colorClassName="text-white"
        />
      ) : (
          <>{label}</>
      )}
    </button>
  )
}

Button.defaultProps = {
  loading: false,
  type: 'button',
}

Button.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.node,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
  type: PropTypes.string,
}

export default Button
