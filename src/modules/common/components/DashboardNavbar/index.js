import React from 'react'
import PropTypes from 'prop-types'

import { MenuAlt3Icon } from '@heroicons/react/outline'

const DashboardNavbar = ({onOpen}) => {
  return (
    <header className="w-full shadow-sm">
      <div className="relative z-10 flex flex-shrink-0 h-16 bg-white">
        <button
          type="button"
          className="order-last px-4 text-gray focus:outline-none md:hidden"
          onClick={onOpen}>
          <span className="sr-only">Open sidebar</span>
          <MenuAlt3Icon className="w-6 h-6" aria-hidden="true" />
        </button>
      </div>
    </header>
  )
}

DashboardNavbar.propTypes = {
  onOpen: PropTypes.func,
}

export default DashboardNavbar
