export const PAGE_PATHS = {
    APP: '/app',
    INCIDENTS: '/app/incidents',
    COMPLETED: '/app/completed',
    SETTINGS: '/app/settings',
    LOGIN: '/app/login'
}
  