import { useCallback, useState, useMemo } from "react";
import to from "await-to-js";
import axios from "axios";
import path from "ramda/src/path";
import getAuthorizationHeaderObject from "modules/auth/utils/getAuthorizationHeaderObject";

/**
 * Hook for executing api query. It uses `axios` internally
 * and a boolean can be passed if mutation is to be done using auth token.
 *
 * @example
 * const {error, data, isLoading, action} = useApiMutation(
 *    client,
 *    'danger',
 *    {withAuthHeaders: true},
 * )
 *
 * @param {string} client The backend service to be called
 * @param {string} resource The rest resource to be queried
 * @param {any} [options] rest api configuration
 */
export default function useApiQuery(
  client,
  resource,
  options
) {
  const [data, setData] = useState(undefined);
  const [error, setError] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const withAuthHeaders = useMemo(
    () => path(["withAuthHeaders"], options) || false,
    [options]
  );

  const action = useCallback(
    async () => {
      setError(null);
      setIsLoading(true);

      const headers = withAuthHeaders ? getAuthorizationHeaderObject() : {};

      const [err, data] = await to(
        axios.get(`${client}${resource}`, {headers})
      );

      setError(err);
      setData(data?.data);
      setIsLoading(false);
    },
    [withAuthHeaders, options, resource]
  );

  const reset = useCallback(async () => {
    setError(undefined);
    setIsLoading(false);
    setData(undefined);
  }, []);

  return useMemo(
    () => ({
      error,
      data,
      isLoading,
      action,
      reset,
    }),
    [error, data, isLoading, action, reset]
  );
}
