import { useCallback, useState, useMemo } from "react";
import to from "await-to-js";
import axios from "axios";
import path from "ramda/src/path";
import getAuthorizationHeaderObject from "modules/auth/utils/getAuthorizationHeaderObject";

/**
 * Hook for executing api mutation. It uses `axios` internally
 * and a boolean can be passed if mutation is to be done using auth token.
 *
 * @example
 * const {error, data, isLoading, action} = useApiMutation(
 *    client,
 *    'danger',
 *    {withAuthHeaders: true},
 *    {verb: 'post', payload: {} }
 * )
 *
 * @param {string} client The backend service to be called
 * @param {string} resource The rest resource to be mutated
 * @param {('post'|'put')} [verb] Api action for the mutation
 * @param {any} [options] rest api configuration
 */
export default function useApiMutation(
  client,
  resource,
  verb = "post",
  options
) {
  const [data, setData] = useState(undefined);
  const [error, setError] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const withAuthHeaders = useMemo(
    () => path(["withAuthHeaders"], options) || false,
    [options]
  );

  const action = useCallback(
    async (payload) => {
      setError(null);
      setIsLoading(true);

      const headers = withAuthHeaders ? getAuthorizationHeaderObject() : {};

      const [err, data] = await to(
        axios[verb](`${client}${resource}`, payload, {headers})
      );

      setError(err);
      setData(data?.data);
      setIsLoading(false);
    },
    [withAuthHeaders, options, resource]
  );

  const reset = useCallback(async () => {
    setError(undefined);
    setIsLoading(false);
    setData(undefined);
  }, []);

  return useMemo(
    () => ({
      error,
      data,
      isLoading,
      action,
      reset,
    }),
    [error, data, isLoading, action, reset]
  );
}
