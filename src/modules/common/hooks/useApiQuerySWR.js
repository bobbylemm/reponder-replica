import {useMemo, useCallback} from 'react'
import useSWR from 'swr'
import axios from 'axios'
import to from 'await-to-js'
import path from 'ramda/src/path'
import omit from 'ramda/src/omit'
import getAuthorizationHeaderObject from 'modules/auth/utils/getAuthorizationHeaderObject'

/**
 * Hook for executing `GET` api requests. It uses `useSWR` internally
 * and all configurations can be passed as an option. Additionally the request
 * A authorization header with a bearer token id can be generated using the flag "withAuthHeaders"
 * inside the options.
 *x`x
 * @example
 *
 * const {data, error} = useApiQuery(
 *   apiConfig.DANGER_SERVICE,
 *   'danger',
 *   {
 *     revalidateOnFocus: false,
 *     withAuthHeaders: true,
 *   }
 * )
 *
 * @param {string} client The backend service to be called
 * @param {string} resource The rest resource to be mutated
 * @param {any} [options] Optional configuration options
 */
export default function useApiQuery(client, resource, options) {  
  const withAuthHeaders = useMemo(
    () => path(['withAuthHeaders'], options) || false,
    [options]
  )
  const swrOptions = useMemo(
    () => omit(['withAuthHeaders'], options),
    [options]
  )

  const action = useCallback(
    async () => {
        const headers = withAuthHeaders
        ? getAuthorizationHeaderObject()
        : {}

        const [err, data] = await to(
            axios.get(`${client}${resource}`, {
              headers,
            })
          );
        return data.data
    },
    [withAuthHeaders]
  )

  return useSWR(
    `${client}${resource}`,
    action,
    swrOptions
  )
}
