import React, { useReducer, createContext } from "react";
import PropTypes from "prop-types";
import actions from "./actions";

const defaultState = {
  data: {
    user: null,
    loggedIn: false,
    loggingIn: false,
    loginError: null
  },
  dispatch: null,
};

const AuthContext = createContext(defaultState);

const authReducer = (state, action) => {
  switch (action.type) {
    case actions.LOGIN: {
      return {
        ...state,
        data: {
          ...state.data,
          loggedIn: true,
          loggingIn: false,
          loginError: null
        },
      };
    }
    case actions.LOGGING_IN: {
      return {
        ...state,
        data: {
          user: null,
          loggedIn: false,
          loggingIn: true,
          loginError: null
        }
      };
    }
    case actions.LOGIN_ERROR: {
      return {
        ...state,
        data: {
          user: null,
          loggedIn: false,
          loggingIn: false,
          loginError: true
        }
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

const AuthProvider = ({ children }) => {
  const [data, dispatch] = useReducer(authReducer, defaultState);

  return (
    <AuthContext.Provider value={{ data, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuthContext = () => {
  const context = React.useContext(AuthContext);
  if (context === undefined) {
    throw new Error("useCount must be used within a CountProvider");
  }
  return context;
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export { AuthProvider, useAuthContext };
