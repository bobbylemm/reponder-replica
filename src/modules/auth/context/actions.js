export default {
  LOGIN: 'login',
  LOGIN_ERROR: 'login_error',
  LOGGING_IN: 'loggingIn',
  LOGOUT: 'logout'
}