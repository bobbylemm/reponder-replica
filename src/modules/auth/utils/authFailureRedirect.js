import {PAGE_PATHS} from 'modules/common/const'

/**
 * Redirects to the login page
 * If the login was unsuccessful.
 *
 * @param {import('next/router').NextRouter} router The nextjs router instance
 */
export default function authFailureRedirect(router) {
  router.replace({
    pathname: PAGE_PATHS.LOGIN
  })
}
