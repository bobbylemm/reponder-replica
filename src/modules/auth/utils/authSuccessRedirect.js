import {PAGE_PATHS} from 'modules/common/const'

/**
 * Redirects to the dashboard page
 * after a successful login operation.
 *
 * @param {import('next/router').NextRouter} router The nextjs router instance
 */
export default function authSuccessRedirect(router) {
  router.replace({
    pathname: PAGE_PATHS.INCIDENTS,
  })
}
