import configs from 'configs/apiConfigs'

/**
 * Set the Authorization Object to be used in making api request
 *
 * @param {string} token Auth token for validating user
 * @returns {void} Returns a promise with an object with the auth token for authorization
 */
export default function getAuthorizationHeaderObject(token) {
  sessionStorage.setItem(configs.jwt.AUTH_TOKEN_NAME, token);
}
