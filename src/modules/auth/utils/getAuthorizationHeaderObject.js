import configs from 'configs/apiConfigs'

/**
 * Get the Authorization Header Object to be used in making api request
 *
 * @returns {{authorization: string}|null} Returns a promise with an object with the auth token for authorization
 */
export default function getAuthorizationHeaderObject() {
  const token = sessionStorage.getItem(configs.jwt.AUTH_TOKEN_NAME);

  if (!token) return null

  return {authorization: `Bearer ${token}`}
}
