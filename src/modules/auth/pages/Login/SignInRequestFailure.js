import React, {useCallback, useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import omit from 'lodash/omit'
import {useRouter} from 'next/router'
import Modal from 'modules/common/components/Modals'
import useModal from 'modules/common/components/Modals/hooks/useModal'
import Notification, {
  NOTIFICATION_TYPE,
} from 'modules/common/components/Notification'
import WarningIcon from '@heroicons/react/solid/ExclamationIcon'

function SignInRequestFailure({error, reset}) {
  const [showNotification, setShowNotification] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const modalState = useModal()
  const router = useRouter()

  const handleCancelButtonClick = useCallback(() => {
    router.replace(router.pathname, {
      query: omit(router.query, 'error'),
    })
    reset()
    console.log('here 2')
    modalState.close()
  }, [router, reset, modalState])

  useEffect(() => {
    setShowNotification(true)
    console.log('here 1')
    modalState.open()
  }, [modalState])

  console.log(modalState, 'modalState')
  if (showNotification) {
    return (
      <Notification message={"error"} type={NOTIFICATION_TYPE.error} />
    )
  }

  return (
    <Modal
      modalState={modalState}
      icon={
        <WarningIcon className="p-2 rounded-full w-70 h-70 bg-red-light text-red-dark" />
      }
      title={'there was an error'}
      content={'error logging in'}
      dismissible={false}
      actionButton={
        <button>Try again</button>
      }
      cancelButton={
        <button onClick={handleCancelButtonClick}>Try again</button>
      }
    />
  )
}

SignInRequestFailure.propTypes = {
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  reset: PropTypes.func,
}

export default SignInRequestFailure
