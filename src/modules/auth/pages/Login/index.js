import React, { useCallback, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { object, string } from "yup";
import { Toaster } from "react-hot-toast";
import path from "ramda/src/path";
import isEmpty from "ramda/src/isEmpty";
import equals from "ramda/src/equals";
import Form, { Input } from "modules/common/components/Form";
import Logo from "modules/common/components/Sidebars/Logo";
import GoogleLogo from "assets/svg/google.svg";

import { useAuthContext } from "modules/auth/context/auth";
import { notify } from "modules/common/components/Notification";
import useApiMutation from "modules/common/hooks/useApiMutation";
import useApiQuery from "modules/common/hooks/useApiQuery";
import configs from "configs/apiConfigs";
import ACTIONS from "../../context/actions";
import { setAuthorizationObject, authSuccessRedirect } from "../../utils";
import Button from "modules/common/components/Button";

const initialValues = { email: "", password: "" };
const validationSchema = object({
  email: string().required("Email is required").email("email is not valid"),
  password: string()
    .required("Password is required")
    .min(6, "Must be at least 6 characters"),
});

const Login = () => {
  const router = useRouter();
  const { data, dispatch } = useAuthContext();
  const {
    action,
    data: authResponse,
    error,
    isLoading: authLoading,
    reset,
  } = useApiMutation(configs.services.AUTH_SERVICE_V1, "auth/login", "post");

  const {
    action: getUserProfile,
    data: userProfileData,
    error: userError,
    isLoading: profileLoading
  } = useApiQuery(configs.services.AUTH_SERVICE_V1, "user/profile", {
    withAuthHeaders: true,
  });

  useEffect(() => {
    (async () => {
      if (authResponse) {
        dispatch({ type: ACTIONS.LOGIN });
        const token = path(["data", "access_token"], authResponse);
        if (token) {
          setAuthorizationObject(token);
          await getUserProfile();
        }
      }
      if (error) {
        dispatch({ type: ACTIONS.LOGIN_ERROR });
        notify({ message: "Invalid Login credentials", type: "error" });
      }
    })();
  }, [authResponse, error]);

  useEffect(() => {
    (async () => {
      if (!isEmpty(userProfileData)) {
        const userType = path(["data", "type"], userProfileData);
        if (userType && equals(userType, "responder")) {
          dispatch({ type: ACTIONS.LOGIN });
          authSuccessRedirect(router);
        } else if (userType && !equals(userType, "responder")) {
          dispatch({ type: ACTIONS.LOGIN_ERROR });
          notify({ message: "Invalid Login credentials", type: "error" });
        }
      }
      if (userError) {
        dispatch({ type: ACTIONS.LOGIN_ERROR });
        notify({ message: "Invalid Login credentials", type: "error" });
      }
    })();
  }, [userProfileData, userError]);

  const handleSubmit = useCallback(
    async (args) => {
      const email = path(["email"], args);
      const password = path(["password"], args);

      await action({ email, password });
    },
    [error]
  );

  return (
    <div className="flex min-h-screen bg-indigo-dark justify-center items-center">
      {/* {data.loginError && (
        <SignInRequestFailure error={"Error logging in"} reset={reset} />
      )} */}
      <Toaster position="top-right" />
      <div className="min-h-screen flex flex-col justify-center py-12 sm:px-6 lg:px-8 w-full">
        <div className="flex justify-center">
          <Logo className="h-28 w-28" />
        </div>

        <div className="sm:mx-auto sm:w-full sm:max-w-md md:w-1/4">
          <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <div className="flex justify-center border border-gray-lightest py-3 rounded-sm">
              <GoogleLogo />
              <p className="ml-2">Login with google</p>
            </div>
            <div className="relative my-4">
              <div className="absolute inset-0 flex items-center">
                <div className="w-full border-t border-gray-lightest" />
              </div>
              <div className="relative flex justify-center text-sm">
                <span className="px-2 bg-white text-black-light">OR</span>
              </div>
            </div>
            <Form
              initialValues={initialValues}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <Input
                label={"Email"}
                name="email"
                placeholder="you@example.com"
                type="email"
              />
              <div className="my-6">
                <Input
                  label={"Password"}
                  name="password"
                  placeholder="*****"
                  type="password"
                />
              </div>
              <div className="flex items-center justify-between mb-4">
                <div className="flex items-center">
                  <input
                    id="remember-me"
                    name="remember-me"
                    type="checkbox"
                    className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 border-2"
                  />
                  <label
                    htmlFor="remember-me"
                    className="ml-2 block text-sm text-gray-900"
                  >
                    Keep me logged in
                  </label>
                </div>
              </div>
              <div>
                <Button
                  type="submit"
                  className="py-4 px-4 border border-transparent rounded-sm text-sm font-medium text-white bg-indigo-dark hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  label="Sign in"
                  loading={authLoading || profileLoading}
                />
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
