import {useCallback} from 'react'
import {useRouter} from 'next/router'
import configs from 'configs/apiConfigs'
import ACTIONS from '../context/actions'

import {
  authSuccessRedirect,
  authFailureRedirect
} from '../utils'
import { useAuthContext } from '../context/auth'

export default function useIsUserLoggedIn() {
  const router = useRouter()
  const {dispatch} = useAuthContext()
  const token = sessionStorage.getItem(configs.jwt.AUTH_TOKEN_NAME)

  const action = useCallback(() => {
    if (token) {
      dispatch({type: ACTIONS.LOGIN})
    } else {
      dispatch({type: ACTIONS.LOGIN_ERROR})
    }
  }, [router])
  action()
}
