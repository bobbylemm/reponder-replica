import React, { useState, useCallback } from "react";
import classNames from "classnames";

const Tabs = ({ items, defaultActiveIndex }) => {
  const [currentActiveIndex, setCurrentActiveIndex] =
    useState(defaultActiveIndex);

    const handleTabChange = useCallback((tabIndex) => {
        setCurrentActiveIndex(tabIndex)
      }, [])
  return (
    <>
      <div className="sm:hidden">
        <label htmlFor="tabs" className="sr-only">
          Select a tab
        </label>
        <select
          id="tabs"
          name="tabs"
          onChange={(e) => handleTabChange(e.target.value)}
          className="block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
          value={currentActiveIndex}
        >
          {items.map((tab) => (
            <option key={tab?.title}>{tab?.title}</option>
          ))}
        </select>
      </div>
      <div className="hidden sm:block">
        <div className="border-b border-gray-lightest">
          <nav className="-mb-px flex space-x-8 px-6" aria-label="Tabs">
            {items.map((tab, tabIndex) => (
              <a
                key={tab?.title}
                onClick={() => handleTabChange(tabIndex)}
                className={classNames(
                  {
                    "border-indigo text-indigo-600": tabIndex === currentActiveIndex,
                    "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300":
                        tabIndex !== currentActiveIndex,
                  },
                  "whitespace-nowrap py-1 px-1 border-b-2 font-medium text-lg cursor-pointer"
                )}
                aria-current={tab?.current ? "page" : undefined}
              >
                {tab?.title}
              </a>
            ))}
          </nav>
        </div>
      </div>
      {items[currentActiveIndex]?.content}
    </>
  );
};

export default Tabs;
