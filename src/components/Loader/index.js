import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import SpinnerIcon from 'assets/svg/spinner.svg'

const Loader = ({className, colorClassName, sizeClassName}) => {
  return (
    <SpinnerIcon
      className={classNames(
        'animate-spin',
        className,
        colorClassName,
        sizeClassName
      )}
    />
  )
}

Loader.defaultProps = {
  sizeClassName: 'w-8 h-8 sm:w-10 sm:h-10 lg:w-14 lg:h-14',
  colorClassName: 'text-indigo',
}

Loader.propTypes = {
  className: PropTypes.string,
  colorClassName: PropTypes.string,
  sizeClassName: PropTypes.string,
}

export default Loader
