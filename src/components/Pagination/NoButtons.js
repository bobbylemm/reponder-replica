/* This example requires Tailwind CSS v2.0+ */
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid'

const NoButtons = () => {
  return (
    <div className="bg-white px-4 py-3 flex items-center justify-center sm:px-6">
      <div className="flex-1 flex justify-between sm:hidden">
        <a
          href="#"
          className="relative inline-flex items-center px-4 py-2 border border-gray-300 text-lg font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
        >
          Previous
        </a>
        <a
          href="#"
          className="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-lg font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
        >
          Next
        </a>
      </div>
      <div className="hidden sm:flex-1 sm:flex sm:items-center justify-center">
        <div>
          <nav className="relative z-0 inline-flex -space-x-px" aria-label="Pagination">
            <a
              href="#"
              className="relative inline-flex items-center px-2 py-2 text-indigo text-base font-medium"
            >
              <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" />
              <span>Previous</span>
            </a>
            {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}
            <a
              href="#"
              aria-current="page"
              className="z-10 relative inline-flex items-center px-4 py-2 text-base font-medium"
            >
              1
            </a>
            <a
              href="#"
              className="text-gray-500 relative inline-flex items-center px-4 py-2 text-base font-medium"
            >
              2
            </a>
            <a
              href="#"
              className="hidden md:inline-flex relative items-center px-4 py-2 text-base font-medium"
            >
              3
            </a>
            <a
              href="#"
              className="relative inline-flex items-center px-2 py-2 text-indigo text-base font-medium"
            >
              <span>Next</span>
              <ChevronRightIcon className="h-5 w-5" aria-hidden="true" />
            </a>
          </nav>
        </div>
      </div>
    </div>
  )
}

export default NoButtons