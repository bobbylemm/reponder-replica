import React, {} from 'react'

export default function Index() {
  return (
    <div>
      <p className="text-4xl">hello there</p>
    </div>
  )
}

export async function getStaticProps({locale}) {
  
  return {props: {layout: {type: 'dashboard'}, isProtected: true}}
}
