import React from 'react'

const DashboardPage = () => {
    return <div>Dashboard page</div>
}

export async function getStaticProps({locale}) {
  
    return {props: {layout: {type: 'dashboard'}, isProtected: true}}
}

export default DashboardPage