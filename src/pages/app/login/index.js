import React from 'react'
import Login from 'modules/auth/pages/Login'

const LoginPage = () => {
    return <Login />
}

export async function getStaticProps({locale}) {
    return {props: {layout: {type: 'none'}, isProtected: false}}
}

export default LoginPage