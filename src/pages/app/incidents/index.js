import React from 'react'
import IncidentsPage from 'modules/dashboard/sections/Incidents'

const Incidents = () => {
    return <IncidentsPage />
}


export async function getStaticProps() {
    return {props: {layout: {type: 'dashboard'}, isProtected: false}}
}

export default Incidents