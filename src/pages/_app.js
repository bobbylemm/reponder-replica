import "styles/globals.css";
import { Toaster } from "react-hot-toast";
import App from "modules/common/components/App";
import { AuthProvider } from "modules/auth/context/auth";

function NextApp({ Component, pageProps, err }) {
  return (
    <AuthProvider>
      <Toaster position="top-right" containerStyle={{zIndex: 10000}} />
      <App Component={Component} pageProps={pageProps} err={err} />
    </AuthProvider>
  );
}

export default NextApp;
