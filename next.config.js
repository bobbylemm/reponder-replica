const path = require('path')
const withPlugins = require('next-compose-plugins')
const nextReactSvg = require('next-react-svg')
const nextConfig = require('./configs/next')

module.exports = withPlugins(
  [
    [
      nextReactSvg,
      {
        include: [
          path.resolve(__dirname, 'src/assets/icons'),
          path.resolve(__dirname, 'src/assets/svg'),
        ],
      },
    ],
  ],
  nextConfig
)
